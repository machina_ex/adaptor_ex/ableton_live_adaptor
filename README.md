# Ableton Live adaptor

Connect your Ableton Liveset to [adaptor:ex](https://adaptorex.org).

Ableton Live adaptor is a max4live device that you add to your liveset to control clips, tracks and scenes via the ableton plugin for adaptor:ex.

Checkout the [documentation page](https://machina_ex.gitlab.io/adaptor_ex/adaptor_ex_tutorials/basics/plugins/ableton.html)

![Screenshot of the max4live adaptor audio device](./adaptor4live.png)

The max4live device sends and receives data via OSC(UDP) on a local network.

## MQTT Version

There is an alternative max4live device `mqttadaptorforlive.amxd` that allows you to use the IoT Network Protocol MQTT instead. It works with the [max-mqtt](https://github.com/256dpi/max-mqtt) object by Joël Gähwiler that is bundled with this repository.

![Screenshot of the max4live mqtt adaptor device](./mqtt_adaptor.png)

To use it you need to place the external files [mqtt.mxo](./Library/mqtt/externals/mqtt.mxo) and [liveset_control.js](./liveset_control.js) into your Liveset project directory alongside the m4l audio device [mqttadaptorforlive.amxd](./mqttadaptorforlive.amxd). Once you did `Collect and Save` you will find the amxd file in the external samples directory.

The MQTT topic to trigger clips, tracks or scenes is the combination of the command address with the base topic. For example:

`adaptorex/ableton/play/clip "Some clip"`

You can change the base topic in the m4l device.

## Incoming messages to the M4L Device

* **play** - fire clips or scenes

`/play/clip "Some clip"`

* **stop** - stop clips or tracks

`/stop/track "4-Some Track"`

* **mix** - change track mixer device values

`/mix "4-Some Track" volume 0.5`

options: 
- `volume` - value between `0.0` and `1.0` 
- `panning` - value between `-1.0` and `1.0`
- `track_activator` - `0` for OFF and `1` for ON
- send channels - channel character `A`, `B` etc. and value between `0.0` and `1.0` 

example: `/mix "4-Some Track" B 0.5`

* **set** - set properties of clips, tracks or scenes (experimental)

## Outgoing messages from the M4L Device

* **clip_playing** - a clip started playing
OSC Message example: 

`/clip_playing "Some clip"`

* **clip_stopped** - a clip is done playing or has been stopped
OSC Message example: 

`/clip_stopped "Some clip"`

* **liveset** - all clips, tracks and scenes in the liveset
OSC Message example: 

`/liveset {clips:["Some clip", "Another clip"], tracks:["1-First Track","2-Second Track"], scenes:["Entrance","Athmo Scene","Something else"]}`

## Further information

If you want to find out how this works take a look into the patcher, the javascript file and check out the Live Object Model:  
https://docs.cycling74.com/max5/refpages/m4l-ref/m4l_live_object_model.html#MixerDevice
