outlets = 3

var liveset = undefined

var buffer = {clips:{},tracks:{},scenes:{}}

function init() {
  liveset = new LiveAPI("live_set")
  preload()
}

function info(text) {
  post(text + "\n")
  outlet(0, "info", text)
}
function warn(text) {
  post(text + "\n")
  outlet(0, "warn", text)
}
function event(event, message) {
  //outlet(2, '{"' + event + '":"' + message + '"}')
  outlet(2, event + ' ' + message)
}

/**
* handle incoming osc message
*/
function route(msg) {
	var js_msg = JSON.parse(msg)
	for(var key in js_msg)
	{
		// info("incoming: " + key + ": " + js_msg[key])
		switch(key) {
			case "trigger":
			case "call":
			case "start":
			case "play":
				if(js_msg[key].hasOwnProperty("scene")) {
					startScene(js_msg[key].scene)
        }
				if (js_msg[key].hasOwnProperty("clip")) {
					startClip(js_msg[key].clip)
				}
        if (typeof js_msg[key] === 'string') {
					startClip(js_msg[key])
			  }
				break
			case "stop":
				if(js_msg[key].hasOwnProperty("track")) {
          stopTrack(js_msg[key].track)
        }
				if(js_msg[key].hasOwnProperty("clip")) {
					stopClip(js_msg[key].clip)
				}
        if(typeof js_msg[key] === "string"){
					stopClip(js_msg[key])
			  }
        break
      case "mix":
        if(js_msg[key].hasOwnProperty("track") && js_msg[key].hasOwnProperty("device") && js_msg[key].hasOwnProperty("value")) {
          setMixer(js_msg[key].track, js_msg[key].device, js_msg[key].value)
        } else {
          warn("not enough info to set mixer device")
        }
        break
      case "set":
        set(js_msg[key])
        break
			default:
				error('no valid command in message', js_msg)
		}
	}
}

function setValue(item, name, property, value) {
  if(buffer.hasOwnProperty(item)) {
    if(buffer[item].hasOwnProperty(name)) {
      if(item == "clips") {
        for(var i = 0; i<buffer[item][name].length;i++) {
          buffer[item][name][i].set(property, value)
          info("set " + item + " " + name + " " + property + " to " + value)
        }
      } else {
        buffer[item][name].set(property, value)
        info("set " + item + " " + name + " " + property + " to " + value)
      }
    } else {
      warn(item + " not found: " + name + ". Hit refresh if you changed or added " + item)
    }
  } else {
    warn("no such item: " + item + ". You can only set properties for clips, scenes or tracks ")
  }
}

function setMixer(track, device, value) {
  track = track.replace(/'"/g,'')

  var mixer = undefined
  var index = -1

  var track_path = buffer.tracks[track].path.replace(/['"]/g,'')

  if(typeof device === 'string') {
    if(device.length == 1) {
      index = device.toUpperCase()
      index = device.charCodeAt(0) - 65
      device = "sends"
    }
  } else {
    index = device
    device = "sends"
  }

  if(index >= 0) {
    mixer = new LiveAPI(track_path + " mixer_device " + device + " " + index)
  } else {
    mixer = new LiveAPI(track_path + " mixer_device " + device)
  }

  if(mixer.id != 0) {
    mixer.set("value", value)
    if(index) {
      info("set " + device + " " + index + " value of track " + track + ": " + mixer.get("value"))
    } else {
      info("set " + device + " value of track " + track + ": " + mixer.get("value"))
    }
  } else if (index) {
    warn("no " + device + " in track " + track + " at index " + index)
  } else {
    warn("no " + device + " in track " + track)
  }
}

function stopTrack(name) {
  if(name.toLowerCase() == "master" || name.toLowerCase() == "all") {
    liveset.call("stop_all_clips")
  } else if (buffer.tracks.hasOwnProperty(name)){
    buffer.tracks[name].call("stop_all_clips")
    info("stop track " + buffer.tracks[name].get("name"))
  } else {
    warn("Track not found: " + name + ". Hit refresh if you changed or added tracks")
  }
}

function startScene(name) {
  if(buffer.scenes.hasOwnProperty(name)) {
    buffer.scenes[name].call("fire")
    info("fire scene " + buffer.scenes[name].get("name"))
  } else {
    warn("Scene not found: " + name + ". Hit refresh if you changed or added scenes")
  }
}

function startClip(name) {
  if(buffer.clips.hasOwnProperty(name)) {
    for(var i = 0; i<buffer.clips[name].length;i++) {
      buffer.clips[name][i].call("fire")
      info("fire " + i+1 + ". clip " + buffer.clips[name][i].get("name"))
      /*
      var playing = false

      function callback(pos) {
        info("TEST")
        info(pos)
        info("Playing: " + this.get("is_playing"))
        if(this.get("is_playing") != 0 && !playing) {
          playing = true
          info(this.get("name") + " playing ")
          //this.property = "playing_position"
        } else if(playing) {
          //info(pos)
          if(pos[1] <= 0) {
            playing = false
            info(this.get("name") + " stopped")
            this.property = "playing_status"
          }
        }
      }

      var observer = new LiveAPI(callback, buffer.clips[name][i].path)
      observer.property = "playing_status"
      */
    }
  } else {
    warn("Clip not found: " + name + ". Hit refresh if you changed or added clips")
  }
}

function stopClip(name) {
  if(buffer.clips.hasOwnProperty(name)) {
    for(var i = 0; i<buffer.clips[name].length;i++) {
      buffer.clips[name][i].call("stop")
      info("stop " + i+1 + ". clip " + buffer.clips[name][i].get("name"))
    }
  } else {
    warn("Clip not found: " + name + ". Hit refresh if you changed or added clips")
  }
}

function clipPlayingStatus() {
  var clip = this.get("name")
  // info(clip + " playing " + this.get("is_playing"))
  if(this.get("is_playing") == 1) {
      info(clip + " playing")
      event("clip_playing", clip)
  } else if(this.get("is_playing") == 0) {
      info(clip + " stopped")
      event("clip_stopped", clip)
  }
}

function preload() {
    buffer = { clips: {}, tracks: {}, scenes: {} }
    var message = { clips: [], tracks: [], scenes: []}

    // Tracks and Clips
    for (var i = 0; i < liveset.get("tracks").length / 2; i++) {
        var track = new LiveAPI("live_set tracks " + i)
        buffer.tracks[track.get("name")] = track
        message.tracks.push(track.get("name")[0])

        for (var c = 0; c < track.get("clip_slots").length / 2; c++) {
            var clipslot = new LiveAPI("live_set tracks " + i + " clip_slots " + c)

            if (clipslot.get("clip")[1] != 0) {
                var clip = new LiveAPI(clipPlayingStatus, "live_set tracks " + i + " clip_slots " + c + " clip")
                info("set listener for " + clip.get("name"))
                clip.property = "playing_status"
                if (buffer.clips.hasOwnProperty(clip.get("name"))) {
                    buffer.clips[clip.get("name")].push(clip)
                } else {
                    buffer.clips[clip.get("name")] = [clip]
                }
                message.clips.push(clip.get("name")[0])
            }
        }
    }

    // Scenes
    for (var i = 0; i < liveset.get("scenes").length / 2; i++) {
        var scene = new LiveAPI("live_set scenes " + i)
        buffer.scenes[scene.get("name")] = scene
        message.scenes.push(scene.get("name")[0])
    }
    
    outlet(1, JSON.stringify(message))
    
    info("Found " + Object.keys(buffer.tracks).length + " tracks, " + Object.keys(buffer.clips).length + " clips and " + Object.keys(buffer.scenes).length + " scenes.")
}